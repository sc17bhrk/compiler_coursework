// represents an unknown subroutine possibly from another class
// its type is unknown as the relevant file may not have been parsed yet

public class UnknownSymbol extends Symbol{
   public String className;
   public String expectedType;
   public String file;

   public UnknownSymbol(String _name, String _type, int _offset,
               int _line_num, String _className, String _file) {
      super(_name, _type, null, _offset, true, _line_num);
      className = _className;
      file = _file;
   }
}
