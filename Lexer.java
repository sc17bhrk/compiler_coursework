import java.io.FileNotFoundException;
import java.io.File;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.IOException;
// Lexer which retrieves tokens ignoring whitespace and comments

public class Lexer {
   // fields
   private String file;
   private int line_num, chars_read;
   private boolean is_comment, is_peeking = false;
   // list of keywords and acceptable symbols
   private static final String[] keywords = {
               "class", "constructor", "method", "function",
               "int", "boolean", "char", "void",
               "var", "static", "field",
               "let", "do", "if", "else", "while", "return",
               "true", "false", "null", "this"
   };
   private static final char[] symbols = {
               '(', ')', '[', ']', '{', '}', ',', ';', '=', '.',
               '+', '-', '*', '/', '&', '|', '~', '<', '>'
   };

   //lexer constructor(s)
   // checks if file to be analysed is valid and initialised fields
   public Lexer(String filename) throws IllegalArgumentException{
      if(!filename.endsWith(".jack")){
         throw new IllegalArgumentException("Invalid filename - not a .jack file");
      }
      file = filename;
      line_num = 1;
      chars_read = 0;
   }

   public Lexer(File filename) throws IllegalArgumentException{
      this(filename.getName());
   }

   public int getLineNum() {
      return line_num;
   }

   // helper method to avoid repeating code
   private int ReadThenPeek(int c, BufferedReader reader) throws IOException {
      // consume the character and then peek at next character
      reader.read();
      ++chars_read;
      reader.mark(1);
      c = reader.read();
      reader.reset();
      return c;
   }

   //helper method to skip whitespace when searching for next token
   private int ConsumeWhitespace(int c, BufferedReader reader) throws IOException {
      while(c != -1 && Character.isWhitespace((char) c)) {
         if(c == '\n' && !is_peeking){
            ++line_num;
         }
         c = ReadThenPeek(c, reader);
      }
      return c;
   }

   // helper method to skip unnecessary comments when searchingfor next token
   private int SkipComments(int c, BufferedReader reader) throws IOException {
      is_comment = false;
      //if character appears to be the start of a comment
      if(c == '/'){
         reader.read(); //reads the first '/' currently stored in c
         ++chars_read;
         reader.mark(1);
         int next_c = reader.read();
         reader.reset();

         //single line comments
         if(next_c == '/'){
            is_comment = true;
            while(next_c != -1 && next_c != '\n') {
               // consume the character and then peek at next character
               reader.read();
               ++chars_read;
               reader.mark(1);
               next_c = reader.read();
               reader.reset();
            }
            c = next_c;
         }

         //multi-line comments
         if(next_c == '*'){
            is_comment = true;
            while(next_c != -1 && !(c == '*' && next_c =='/')){
               // read the first character and then peek at next character
               // these two characters are checked to see if they signify the
               // end of the multi-line comment, i.e. */
               c = reader.read();
               ++chars_read;
               if(c == '\n' && !is_peeking){
                  ++line_num;
               }
               reader.mark(1);
               next_c = reader.read();
               reader.reset();
               //reached end of file before end of multi-line comment - e.g. /*end
               if(next_c == -1) {
                  reader.close(); //about to throw error so close the file/buffer
                  (new Error(line_num)).UnexpectedEOFException('*');
               }
            }
            //consuming the last '/' character in the multi-line comment
            c = ReadThenPeek(c, reader);
         }
      }
      return c;
   }

   //gets the next token from the file - either a keyword, id, integer,
   // string_literal, symbol or eof token
   public Token GetNextToken() {
      is_comment = true;
      Token t = new Token();
      try {
         BufferedReader reader = new BufferedReader(
                                    new InputStreamReader(
                                       new FileInputStream(file)));

         // set the reader to where the last token was read from
         reader.skip(chars_read);
         //peeking at first character in stream
         reader.mark(1);
         int c = reader.read();
         reader.reset();

         //get rid of any whitespace from current position to next token
         c = ConsumeWhitespace(c, reader);

         // if the character may be the start of a comment
         // skip the comment and any consecutive comments
         while(is_comment){
            c = SkipComments(c, reader);
            c = ConsumeWhitespace(c, reader);
         }


         // eof token
         if(c == -1){
            t.lexeme += (char) c;
            t.tokenType = Token.TokenTypes.eof;
            reader.close(); //reached end of function so close the file/buffer
            return t;
         }

         // integer token
         if(Character.isDigit((char) c)){
            while(c != -1 && Character.isDigit((char) c)) {
               t.lexeme += (char) c;
               c = ReadThenPeek(c, reader);
            }
            t.tokenType = Token.TokenTypes.integer;
            reader.close(); //reached end of function so close the file/buffer
            return t;
         }

         // keyword or identifier token
         if(Character.isLetter((char) c) ||  c == '_'){
            while(c != -1 && (Character.isLetterOrDigit((char) c) ||  c == '_')) {
               t.lexeme += (char) c;
               c = ReadThenPeek(c, reader);
            }
            //classifying the token type, either a keyword or identifier
            for(String keyword : keywords){
               if(t.lexeme.equals(keyword)){
                  t.tokenType = Token.TokenTypes.keyword;
                  reader.close(); //reached end of file so close the file/buffer
                  return t;
               }
            }
            //if the lexeme is none of the above it must be an identifier
            t.tokenType = Token.TokenTypes.id;
            reader.close(); //reached end of function so close the file/buffer
            return t;
         }

         // string_literal token
         if( c == '\"'){
            reader.read();
            ++chars_read;
            reader.mark(1);
            c = reader.read();
            reader.reset();
            while(c != -1 &&  c != '\"') {
               t.lexeme += (char) c;
               c = ReadThenPeek(c, reader);
               //reached end of file before closing string literal - e.g. "end
               if(c == -1) {
                  reader.close(); //about to throw error so close the file/buffer
                  (new Error(line_num)).UnexpectedEOFException('\"');
               }
            }
            //consuming last speech char (using if statement in case last char was eof)
            if(c == '\"'){
               reader.read();
               ++chars_read;
            }
            t.tokenType = Token.TokenTypes.string_literal;
            reader.close(); //reached end of function so close the file/buffer
            return t;
         }

         // symbol token
         /*at this point, the character is not the start of a comment,
         or whitespace, or a digit, or the start of some bool const, null,
         string literal, identifier or keyword so it must either be a symbol or
         invalid character
         */
         reader.read();
         ++chars_read;
         t.lexeme += (char) c;
         for(char sym : symbols){
            // if(t.lexeme.equals(sym)){
            if(c == sym){
               t.tokenType = Token.TokenTypes.symbol;
               reader.close(); //reached end of function so close the file/buffer
               return t;
            }
         }

         /*at this point, if no token has been returned the character peeked from
         the stream is an invalid character in the JACK language so the lexer throws an error
         */
         reader.close(); //reached end of function so close the file/buffer
         (new Error(line_num)).IllegalCharacterException((char)c);
      }
      catch(FileNotFoundException ex) {
         System.out.printf("Error, %s not found\n", file);
      }
      catch(IOException ex) {
         System.out.println("Error, IOException occurred");
         ex.printStackTrace();
      }
      return t;
   }

   // peeks and returns next token but doesn't remove from the stream
   public Token PeekNextToken() {
      // let lexer know of peeking, to not increment whitespace
      is_peeking = true;
      Token t = new Token();
      //save the current value of characters/tokens read to restore later
      int cur_chars_read = chars_read;

      t = GetNextToken();
      //chars_read has been altered via GetNextToken() and is set to the previous value
      chars_read = cur_chars_read;
      is_peeking = false;
      return t;
   }

}
