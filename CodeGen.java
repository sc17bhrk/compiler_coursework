import java.util.ArrayList;
import java.io.PrintWriter;
import java.io.FileOutputStream;
import java.io.File;
import java.io.IOException;

// class to generate the code
public class CodeGen {
   private ArrayList<String> code;
   private int labelNum;

   public CodeGen() {
      code = new ArrayList<String>();
      labelNum = 0;
   }

   // generates the next unique label
   public String getLabel() {
      return String.format("label%s", String.valueOf(labelNum++));
   }

   // adds instruction to list of instructions
   public void write(String instr) {
      code.add(instr);
   }

   public void write(String instr, Symbol s) {
      write(getInstr(instr, s));
   }

   // a push statement depending on the offest and segment the symbol is stored in
   public String getInstr(String instr, Symbol s) {
      String segment = "local";
      if (s.kind == Symbol.Kind._static) {
         segment = "static";
      }
      else if (s.kind == Symbol.Kind._argument) {
         segment = "argument";
      }
      else if (s.kind == Symbol.Kind._field) {
         segment = "this";
      }
      return String.format("%s %s %s", instr, segment, String.valueOf(s.offset));
   }


   // writes instructions in list to the file
   public void writeToFile(SymbolTables symTables, String dir) {
      String filename, instr;
      // for (String line : code) {
      for (int i = 0, j = 0; i < code.size(); ++i) {
         instr = code.get(i);
         if (instr.startsWith("CLASS")) {
            filename = dir + "/" + instr.split(" ")[1] + ".vm";
            try {
               PrintWriter writer = new PrintWriter(new FileOutputStream(new File(filename)));
               // for (String instr : code) {
               for (j = i + 1; j < code.size(); ++j) {
                  instr = code.get(j);
                  if (instr.startsWith("CLASS")) {
                     break;
                  }
                  if (instr.startsWith("function")) {
                     writeFuncInstr(writer, instr, symTables);
                  }
                  else if (instr.startsWith("call")) {
                     writeCallInstr(writer, instr, symTables);
                  }
                  else if (instr.startsWith("do")) {
                     writeDoInstr(writer, instr, symTables);
                  }
                  else {
                     // no need to modifiy the instruction, write to file
                     writer.println(instr);
                  }
               }
               writer.close();
            }
            catch(IOException e) {
               System.out.println("IOException occured while trying to write code instructions");
            }
         }
         // skip to next instruction, the CLASS
         i = --j;
      }
   }

   // some of the instructions on the list need further information
   // these are evaluated here
   
   // instr in list of the form "function classRef.sub_name"
   // need to append number of local vars used before writing to file
   public void writeFuncInstr(PrintWriter writer, String instr, SymbolTables symTables) {
      String temp, classRef, sub_name;
      temp = instr.split(" ")[1];
      classRef = temp.split("\\.")[0];
      sub_name = temp.split("\\.")[1];

      if (sub_name.equals("new")) {
         SymbolTable classSymTable = symTables.getClassSub(classRef, classRef);
         SymbolTable newSymTable = symTables.getClassSub(classRef, "new");
         int numLocalVars = newSymTable.getNumLocalVars();
         int numClassVars = classSymTable.getNumLocalVars();
         writer.println(instr + " " + String.valueOf(numLocalVars));
         writer.println("push constant " + String.valueOf(numClassVars));
         writer.println("call Memory.alloc 1");
         writer.println("pop pointer 0");
         return;
      }

      if (symTables.getClassSub(classRef, sub_name) == null) {
      }
      SymbolTable symTable = symTables.getClassSub(classRef, sub_name);
      int numLocalVars = symTable.getNumLocalVars();
      writer.println(instr + " " + String.valueOf(numLocalVars));
      if (symTable.kind == SymbolTable.Kind._method) {
         writer.println("push argument 0");
         writer.println("pop pointer 0");
      }
   }

   // instr in list of the form "call classRef.sub_name"
   // need to append number of expected arguments used before writing to file
   public void writeCallInstr(PrintWriter writer, String instr, SymbolTables symTables) {
      String temp, classRef, sub_name, file;
      temp = instr.split(" ")[1];
      classRef = temp.split("\\.")[0];
      sub_name = temp.split("\\.")[1];
      file = instr.split(" ")[2];

      if (classRef.equals("next")) {
      }
      classRef = symTables.getClassRef(classRef, sub_name, file);
      SymbolTable symTable = symTables.getClassSub(classRef, sub_name);
      int numArgs = symTable.getNumArgs();
      // not including 'this' for constructors
      if (symTable.kind == SymbolTable.Kind._constructor) {
         --numArgs;
      }
      instr = String.format("call %s.%s %d", classRef, sub_name, numArgs);
      writer.println(instr);
   }

   // instr in list of the form "do classRef.sub_name"
   public void writeDoInstr(PrintWriter writer, String instr, SymbolTables symTables) {
      String temp, classRef, sub_name, file;
      temp = instr.split(" ")[1];
      classRef = temp.split("\\.")[0];
      sub_name = temp.split("\\.")[1];

      ClassTable cTable = symTables.getClassTable(classRef);
      if (cTable != null) {
         // check the subroutine kind
         SymbolTable symTable = symTables.getSymbolTable(cTable, sub_name);
         if (symTable != null) {
            if (symTable.kind == SymbolTable.Kind._method) {
               writer.println("push pointer 0");
            }
         }
      }
      else {
         file = instr.split(" ")[2];
         cTable = symTables.getClassTable(file);
         if (cTable != null) {
            // if the classname is actually an identifier which is a reference to another class
            Symbol sym = symTables.searchClass(classRef, cTable);
            if (sym != null) {
               writer.println(getInstr("push", sym));
            }
         }
      }
   }
}
