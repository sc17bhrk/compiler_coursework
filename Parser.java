import java.io.File;

public class Parser {
   private File dir;
   private Lexer lexer;
   private SymbolTables symTables;
   private UnknownSymbolTable unknownSyms;
   private SubTables subTables;
   private CodeGen code;

   // constants
   private static final Token.TokenTypes id = Token.TokenTypes.id;

   private static final Symbol.Kind _static = Symbol.Kind._static;
   private static final Symbol.Kind _field = Symbol.Kind._field;
   private static final Symbol.Kind _var = Symbol.Kind._var ;
   private static final Symbol.Kind _argument = Symbol.Kind._argument;

   private static final SymbolTable.Kind _class = SymbolTable.Kind._class;
   private static final SymbolTable.Kind _constructor = SymbolTable.Kind._constructor;
   private static final SymbolTable.Kind _method = SymbolTable.Kind._method;
   private static final SymbolTable.Kind _function = SymbolTable.Kind._function;
   private static final SymbolTable.Kind _if = SymbolTable.Kind._if;
   private static final SymbolTable.Kind _while = SymbolTable.Kind._while;

   // constructor - initialises tables
   public Parser(File dir) throws IllegalArgumentException{
      this.dir = dir;
      symTables = new SymbolTables();
      unknownSyms = new UnknownSymbolTable();
      subTables = new SubTables();
      code = new CodeGen();
   }

   public void run() {
      //iterates over each of the files in the directory and runs every jack
      // file through the lexer, extracting and printing all the tokens
      for (File file: dir.listFiles()) {
         if (file.getName().endsWith(".jack")) {
            //initialise lexer for the file
            lexer = new Lexer(file.getPath());
            // begin parsing the file
            classDeclar();
         }
      }

      // semantic analysis
      Semantics sem = new Semantics(symTables);
      // checking that each symbol type is a valid type
      sem.checkVarTypesValid();
      // checking that each constructor/method/function contains a return statement
      sem.checkRetStmtsExist();
      unknownSyms = sem.getUnknownExprTypes(unknownSyms);
      subTables = sem.replaceUnknownTypes(subTables, unknownSyms);
      sem.checkSubArgsMatch(subTables);

      // code generation - writing to file
      code.writeToFile(symTables, dir.getPath());
   }

   private void classDeclar() {
      Token next_token = lexer.GetNextToken();
      if (next_token.lexeme.equals("class")) {
      }
      else {
         (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
            invalid_lexeme("class", "keyword", next_token.lexeme);
      }
      String table_name = identifier();
      code.write("CLASS " + table_name);

      next_token = lexer.GetNextToken();
      if (next_token.lexeme.equals("{")){
         // entered new scope
         symTables.addClass(table_name);
      }
      else {
         (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
            invalid_lexeme("{", "symbol", next_token.lexeme);
      }

      next_token = lexer.PeekNextToken();
      while (next_token.lexeme.equals("static") ||
            next_token.lexeme.equals("field") ||
            next_token.lexeme.equals("constructor") ||
            next_token.lexeme.equals("function") ||
            next_token.lexeme.equals("method"))
      {
         memberDeclar();
         next_token = lexer.PeekNextToken();
      }

      next_token = lexer.GetNextToken();
      if (next_token.lexeme.equals("}")){
      }
      else{
         (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
            invalid_lexeme("}", "symbol", next_token.lexeme);
      }
   }

   private void memberDeclar() {
      Token next_token = lexer.PeekNextToken();
      if (next_token.lexeme.equals("static") ||
         next_token.lexeme.equals("field"))
      {
         classVarDeclar();
      }
      else if (next_token.lexeme.equals("constructor") ||
              next_token.lexeme.equals("function") ||
              next_token.lexeme.equals("method"))
      {
         subDeclar();
      }
      else {
         (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
            invalid_lexeme("static' or 'field' or 'constructor' or 'function' or 'method",
               "keyword", next_token.lexeme);
      }
   }

   private void classVarDeclar() {
      String var_type, var_name;
      Symbol.Kind kind = null;
      Token next_token = lexer.GetNextToken();
      if (next_token.lexeme.equals("static")) {
         kind = _static;
      }
      else if (next_token.lexeme.equals("field")) {
         kind = _field;
      }
      else {
         (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
            invalid_lexeme("static' or 'field", "keyword", next_token.lexeme);
      }
      var_type = type();
      var_name = identifier();
      // checking if this variable already exists in the symbol table - current scope
      if (symTables.findLocalScope(var_name) != null) {
         (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
            redeclare(var_name);
      }
      // add symbol to table
      symTables.addSymbol(var_name, var_type, kind, lexer.getLineNum());

      next_token = lexer.PeekNextToken();
      while (next_token.lexeme.equals(",")) {
         // consume the token
         lexer.GetNextToken();
         var_name = identifier();
         // checking if this variable already exists in the symbol table
         if (symTables.findLocalScope(var_name) != null) {
            (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
               redeclare(var_name);
         }
         // add symbol to table
         symTables.addSymbol(var_name, var_type, kind, lexer.getLineNum());
         next_token = lexer.PeekNextToken();
      }
      next_token = lexer.GetNextToken();
      if (next_token.lexeme.equals(";")) {
      }
      else {
         (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
            invalid_lexeme(";", "symbol", next_token.lexeme);
      }
   }

   private String type() {
      Token next_token = lexer.GetNextToken();
      if (next_token.lexeme.equals("int")) {
      }
      else if (next_token.lexeme.equals("char")) {
      }
      else if (next_token.lexeme.equals("boolean")) {
      }
      else if (next_token.tokenType == id) {
      }
      else {
         (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
            invalid_lexeme(
               "int' or 'char' or 'boolean' or an identifier", "keyword",
               next_token.lexeme
            );
      }
      return next_token.lexeme;
   }

   // subroutine declaration
   private void subDeclar() {
      String sub_name, sub_type;
      SymbolTable.Kind kind = null;
      Token next_token = lexer.GetNextToken();
      if (next_token.lexeme.equals("constructor")) {
         kind = _constructor;
      }
      else if (next_token.lexeme.equals("function")) {
         kind = _function;
      }
      else if (next_token.lexeme.equals("method")) {
         kind = _method;
      }
      else {
         (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
            invalid_lexeme(
               "constructor' or 'function' or 'method", "keywords",
               next_token.lexeme
            );
      }

      next_token = lexer.PeekNextToken();
      if (next_token.lexeme.equals("void")) {
         // consume the token
         lexer.GetNextToken();
         sub_type = "void";
      }
      else {
         sub_type = type();
      }
      sub_name = identifier();

      // if the constructor has been declared incorrectly with wrong type
      if (kind == _constructor && !sub_type.equals(symTables.getClassName())) {
         (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
            constructor(sub_name, sub_type, symTables.getClassName());
      }

      // adding a new table to list of symbol tables
      symTables.addTable(sub_name, sub_type, kind, lexer.getLineNum());

      // code gen - currently don't know number of local variables, will resolve later
      code.write(String.format("function %s.%s", symTables.getClassName(), sub_name));

      next_token = lexer.GetNextToken();
      if (next_token.lexeme.equals("(")) {
      }
      else {
         (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
            invalid_lexeme("(", "symbol", next_token.lexeme);
      }
      paramList();

      next_token = lexer.GetNextToken();
      if (next_token.lexeme.equals(")")) {
      }
      else {
         (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
            invalid_lexeme(")", "symbol", next_token.lexeme);
      }
      subBody();
   }

   private void paramList() {
      String param_name, param_type;
      Token next_token = lexer.PeekNextToken();

      // it is fine for paramList to be empty
      if (next_token.lexeme.equals("int") || next_token.lexeme.equals("char") ||
         next_token.lexeme.equals("boolean") || next_token.tokenType == id) {
         param_type = type();
         param_name = identifier();
         // adding argument symbol to symbol table
         symTables.addSymbol(param_name, param_type, _argument, true, lexer.getLineNum());

         // can have more identifiers
         next_token = lexer.PeekNextToken();
         while (next_token.lexeme.equals(",")) {
            // consume the token
            lexer.GetNextToken();
            param_type = type();
            param_name = identifier();
            // adding argument symbol to symbol table
            symTables.addSymbol(param_name, param_type, _argument, true, lexer.getLineNum());
            next_token = lexer.PeekNextToken();
         }
      }
   }

   // subroutine body
   private void subBody() {
      zeroOrMoreStmnts();
   }

   private void statement() {
      Token next_token = lexer.PeekNextToken();
      if (next_token.lexeme.equals("var")) {
         varDeclarStmnt();
      }
      else if (next_token.lexeme.equals("let")) {
         letStmnt();
      }
      else if (next_token.lexeme.equals("if")) {
         ifStmnt();
      }
      else if (next_token.lexeme.equals("while")) {
         whileStmnt();
      }
      else if (next_token.lexeme.equals("do")) {
         doStmnt();
      }
      else if (next_token.lexeme.equals("return")) {
         returnStmnt();
      }
      else {
         (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
            invalid_lexeme("var', 'let', 'if', 'while', 'do' or 'return",
               "statement starting with", next_token.lexeme);
      }
   }

   private void varDeclarStmnt() {
      String var_name, var_type;
      Token next_token = lexer.GetNextToken();
      if (next_token.lexeme.equals("var")) {
      }
      else {
         (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
            invalid_lexeme("var", "keyword", next_token.lexeme);
      }
      var_type = type();
      var_name = identifier();
      // checking if this variable already exists in the symbol table
      if (symTables.findLocalScope(var_name) != null) {
         (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
            redeclare(var_name);
      }
      // add symbol to table
      // ints and chars are initialised to 0
      if (var_type.equals("int") || var_type.equals("char")) {
         symTables.addSymbol(var_name, var_type, _var, true, lexer.getLineNum());
      }
      else {
         symTables.addSymbol(var_name, var_type, _var, lexer.getLineNum());
      }

      //declaring multiple variables
      next_token = lexer.PeekNextToken();
      while (next_token.lexeme.equals(",")) {
         // consume the token
         lexer.GetNextToken();
         var_name = identifier();
         // checking if this variable already exists in the symbol table
         if (symTables.findLocalScope(var_name) != null) {
            (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
               redeclare(var_name);
         }
         // add symbol to table
         // ints and chars are initialised to 0
         if (var_type.equals("int") || var_type.equals("char")) {
            symTables.addSymbol(var_name, var_type, _var, true, lexer.getLineNum());
         }
         else {
            symTables.addSymbol(var_name, var_type, _var, lexer.getLineNum());
         }
         next_token = lexer.PeekNextToken();
      }
      next_token = lexer.GetNextToken();
      if (next_token.lexeme.equals(";")) {
      }
      else {
         (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
            invalid_lexeme(";", "symbol", next_token.lexeme);
      }
   }

   private void letStmnt() {
      String var_name, type;
      Symbol curSymbol;
      Boolean accessArr = false;
      Token next_token = lexer.GetNextToken();
      if (next_token.lexeme.equals("let")) {
      }
      else {
         (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
            invalid_lexeme("let", "keyword", next_token.lexeme);
      }
      var_name = identifier();
      // checking if this variable doesn't exist in the symbol tables
      if ((curSymbol = symTables.find(var_name)) == null) {
         (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
            not_declared(var_name);
      }

      next_token = lexer.PeekNextToken();
      if (next_token.lexeme.equals("[")) {
         accessArr = true;
         // checking if this variable is not an array in the symbol tables
         if (!curSymbol.type.equals("Array")) {
            (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
               invalid_type(var_name, "Array");
         }
         // consume token
         lexer.GetNextToken();
         // ensuring that array index expression is an integer
         type = expression();
         if (!type.equals("int") && !type.equals("char")) {
            (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
               array_index(var_name + "[not_an_int]", type);
         }
         next_token = lexer.GetNextToken();
         if (next_token.lexeme.equals("]")) {
         }
         else {
            (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
               invalid_lexeme("]", "symbol", next_token.lexeme);
         }

         // at this point the array index value should be pushed to the stack
         // need to add to start of array pointer to get new address
         code.write("push", curSymbol);
         // add the array index to the pointer to start of array
         code.write("add");
      }

      next_token = lexer.GetNextToken();
      if (next_token.lexeme.equals("=")) {
      }
      else {
         (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
            invalid_lexeme("=", "symbol", next_token.lexeme);
      }
      type = expression();
      // check if types are compatible for let stmnt
      if (Semantics.checkTypesValid(curSymbol.type, type, "=")) {
         if (type.contains(" UNKNOWN ")) {
            unknownSyms.expectType(unknownSyms.exprNum(), curSymbol.type);
         }
      }
      else {
         (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
            incompatible_type_operation(next_token.lexeme, curSymbol.type, type);
      }

      next_token = lexer.GetNextToken();
      if (next_token.lexeme.equals(";")) {
         // showing a value has been assigned to the variable
         symTables.assignedValue(var_name);
      }
      else {
         (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
            invalid_lexeme(";", "symbol", next_token.lexeme);
      }

      // code generation - at this point the RHS expression value should be on top of the stack
      if (accessArr) {
         code.write("pop temp 0");
         // temp0 = value of RHS expression
         // top stack value = RAM address of array[expr]

         // store location of array pointer in 'that' pointer mem. loc.
         code.write("pop pointer 1");
         code.write("push temp 0");
         // put value into specified array position
         code.write("pop that 0");
      }
      else {
         code.write("pop", curSymbol);
      }
   }

   private void ifStmnt() {
      Token next_token = lexer.GetNextToken();
      if (next_token.lexeme.equals("if")) {
      }
      else {
         (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
            invalid_lexeme("if", "keyword", next_token.lexeme);
      }

      next_token = lexer.GetNextToken();
      if (next_token.lexeme.equals("(")) {
      }
      else {
         (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
            invalid_lexeme("(", "symbol", next_token.lexeme);
      }
      String type = expression();
      if (Semantics.checkTypesValid(type, "boolean", "")) {
         if (type.contains(" UNKNOWN ")) {
            unknownSyms.expectType(unknownSyms.exprNum(), "boolean");
         }
      }
      else {
         (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
            not_bool_expr(type);
      }

      next_token = lexer.GetNextToken();
      if (next_token.lexeme.equals(")")) {
      }
      else {
         (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
            invalid_lexeme(")", "symbol", next_token.lexeme);
      }
      symTables.addTable("statement", "", _if, lexer.getLineNum());

      // code gen - check boolean expression, skip to end if necessary
      String endIf = code.getLabel();
      String skipElse = code.getLabel();
      code.write("not");
      code.write("if-goto " + endIf);
      // execute statements in if block
      zeroOrMoreStmnts();
      code.write("goto " + skipElse);
      code.write("label " + endIf);

      // can have 0 or 1 else statement
      next_token = lexer.PeekNextToken();
      if (next_token.lexeme.equals("else")) {
         // consume token
         lexer.GetNextToken();
         symTables.addTable("else statement", "", _if, lexer.getLineNum());
         zeroOrMoreStmnts();
      }
      // label to skip the else block if the if block is executed
      code.write("label " + skipElse);
   }

   private void whileStmnt() {
      Token next_token = lexer.GetNextToken();
      if (next_token.lexeme.equals("while")) {
      }
      else {
         (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
            invalid_lexeme("while", "keyword", next_token.lexeme);
      }
      // code gen - add a label for start of loop
      String start = code.getLabel();
      String end = code.getLabel();
      code.write("label " + start);

      next_token = lexer.GetNextToken();
      if (next_token.lexeme.equals("(")) {
      }
      else {
         (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
            invalid_lexeme("(", "symbol", next_token.lexeme);
      }
      String type = expression();
      if (Semantics.checkTypesValid(type, "boolean", "")) {
         if (type.contains(" UNKNOWN ")) {
            unknownSyms.expectType(unknownSyms.exprNum(), "boolean");
         }
      }
      else {
         (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
            not_bool_expr(type);
      }

      next_token = lexer.GetNextToken();
      if (next_token.lexeme.equals(")")) {
      }
      else {
         (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
            invalid_lexeme(")", "symbol", next_token.lexeme);
      }
      symTables.addTable("statement", "", _while, lexer.getLineNum());

      // code gen - check boolean condition, skip to end of loop if false
      code.write("not");
      code.write("if-goto " + end);
      // execute statements in loop
      zeroOrMoreStmnts();
      // code gen - go to start of loop and have end label
      code.write("goto " + start);
      code.write("label " + end);
   }

   private void doStmnt() {
      Token next_token = lexer.GetNextToken();
      if (next_token.lexeme.equals("do")) {
      }
      else {
         (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
            invalid_lexeme("do", "keyword", next_token.lexeme);
      }
      subCall();

      next_token = lexer.GetNextToken();
      if (next_token.lexeme.equals(";")) {
      }
      else {
         (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
            invalid_lexeme(";", "symbol", next_token.lexeme);
      }
      // code gen pop resulting value of stack into temp segment
      code.write("pop temp 0");
   }

   // subroutine call
   private void subCall() {
      String classRef, sub_name;
      classRef = symTables.getClassName();
      sub_name = identifier();
      Token next_token = lexer.PeekNextToken();
      if (next_token.lexeme.equals(".")) {
         // consume token
         lexer.GetNextToken();
         // subcall is of the form do class.sub_name(...)
         classRef = sub_name;
         sub_name = identifier();
      }
      // add called subroutine to table of called subs
      subTables.addTable(classRef, sub_name, lexer.getLineNum(), symTables.getClassName());
      Symbol sym = symTables.find(classRef);
      if (sym != null) {
         code.write("push", sym);
      }
      else {
         code.write(String.format("do %s.%s", classRef, sub_name));
      }

      next_token = lexer.GetNextToken();
      if (next_token.lexeme.equals("(")) {
      }
      else {
         (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
            invalid_lexeme("(", "symbol", next_token.lexeme);
      }
      exprList();
      subTables.goToParentTable();

      next_token = lexer.GetNextToken();
      if (next_token.lexeme.equals(")")) {
      }
      else {
         (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
            invalid_lexeme(")", "symbol", next_token.lexeme);
      }

      // code generation
      // at this point any arguments for a function call will have been
      // pushed onto the stack, so call the function
      code.write(String.format("call %s.%s %s", classRef, sub_name, symTables.getClassName()));
   }

   private void exprList() {
      Token next_token = lexer.PeekNextToken();
      // can have 0 or 1 list of expressions
      if (next_token.lexeme.equals("-") || next_token.lexeme.equals("~") ||
         next_token.tokenType == Token.TokenTypes.integer ||
         next_token.tokenType == Token.TokenTypes.id ||
         next_token.tokenType == Token.TokenTypes.string_literal ||
         next_token.lexeme.equals("true") ||
         next_token.lexeme.equals("false") ||
         next_token.lexeme.equals("null") ||
         next_token.lexeme.equals("this") ||
         next_token.lexeme.equals("("))
      {
         String type = expression();
         // add the type of the expression sent to the sub table
         subTables.addArgType(type);
         //can have multiple expressions
         next_token = lexer.PeekNextToken();
         while (next_token.lexeme.equals(",")) {
            // consume the token
            lexer.GetNextToken();
            type = expression();
            subTables.addArgType(type);
            next_token = lexer.PeekNextToken();
         }
      }
   }

   //return statement
   private void returnStmnt() {
      SymbolTable.Kind kind = symTables.curTable().kind;
      String type = symTables.getCurSubType();
      String return_type = "void";

      Token next_token = lexer.GetNextToken();
      if (next_token.lexeme.equals("return")) {
         symTables.hasReturn();
      }
      else {
         (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
            invalid_lexeme("return", "keyword", next_token.lexeme);
      }

      // can have 0 or 1 expressions
      next_token = lexer.PeekNextToken();

      // if subroutine is a constructor, return statement should be 'return this;'
      if (kind == _constructor && !next_token.lexeme.equals("this")) {
         (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
            return_should_be_this();
      }
      if (next_token.lexeme.equals("-") || next_token.lexeme.equals("~") ||
         next_token.tokenType == Token.TokenTypes.integer ||
         next_token.tokenType == Token.TokenTypes.id ||
         next_token.tokenType == Token.TokenTypes.string_literal ||
         next_token.lexeme.equals("true") ||
         next_token.lexeme.equals("false") ||
         next_token.lexeme.equals("null") ||
         next_token.lexeme.equals("this") ||
         next_token.lexeme.equals("("))
      {
         return_type = expression();
      }

      next_token = lexer.GetNextToken();
      if (next_token.lexeme.equals(";")) {
         // checking the return type matches with type of func/method/constr
         if (Semantics.checkTypesValid(type, return_type, "")) {
            if (return_type.contains(" UNKNOWN ")) {
               unknownSyms.expectType(unknownSyms.exprNum(), type);
            }
         }
         else {
            (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
               invalid_return_type(type, return_type);
         }
      }
      else {
         (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
            invalid_lexeme(";", "symbol", next_token.lexeme);
      }

      if (kind == _constructor || kind == _method || kind == _function) {
         next_token = lexer.PeekNextToken();
         // the return statement should be the last statement in these subroutines
         if (!next_token.lexeme.equals("}")) {
            (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
               return_not_last_stmt();
         }
      }
      // if the return is not in a subroutine or if stmnt - i.e. unconditional return
      else if (kind != _if) {
         (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
            unconditional_return();
      }

      // code gen - at this point, there may or may not have been an expression
      // value pushed onto the stack - if it wasn't void, just need return code
      if (return_type.equals("void")) {
         code.write("push constant 0");
      }
      code.write("return");
   }

   private String expression() {
      String type1, type2;
      type1 = relaExp();
      // 0 or more relational expressions with boolean operators
      Token next_token = lexer.PeekNextToken();
      while (next_token.lexeme.equals("&") || next_token.lexeme.equals("|")) {
         // consume the token
         lexer.GetNextToken();
         type2 = relaExp();
         // check if types are compatible and swap if necessary
         if (Semantics.checkTypesValid(type1, type2, next_token.lexeme)){
            // swapping t1 to the known type for more rigorous type checking
            if (type1.contains(" UNKNOWN ") && !type2.contains(" UNKNOWN ")) {
               unknownSyms.expectType(unknownSyms.exprNum(), type2);
               type1 = type2;
            }
            else if (!type1.contains(" UNKNOWN ") && type2.contains(" UNKNOWN ")) {
               unknownSyms.expectType(unknownSyms.exprNum(), type1);
            }
            // the last two unknowns need their expected types from the operation
            else if (type1.contains(" UNKNOWN ") && type2.contains(" UNKNOWN ")) {
               unknownSyms.expectType(unknownSyms.exprNum() - 1, null, next_token.lexeme);
               unknownSyms.expectType(unknownSyms.exprNum() , null, next_token.lexeme);
            }
         }
         else {
            (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
               incompatible_type_operation(next_token.lexeme, type1, type2);
         }

         // code generation
         if (next_token.lexeme.equals("&")) {
            code.write("and");
         }
         else {
            code.write("or");
         }

         next_token = lexer.PeekNextToken();
      }
      return type1;
   }

   //relational express
   private String relaExp() {
      String type1, type2;
      boolean isBoolExp = false;
      type1 = arithExp();
      // 0 or more arithmetic expressions with (in)equality operators
      Token next_token = lexer.PeekNextToken();
      while (next_token.lexeme.equals("=") || next_token.lexeme.equals(">") ||
            next_token.lexeme.equals("<")) {
         // consume the token
         lexer.GetNextToken();
         // this relaExp is a boolean expression
         isBoolExp = true;
         type2 = arithExp();
         // check if types are compatible and swap if necessary
         if (Semantics.checkTypesValid(type1, type2, next_token.lexeme)){
            // swapping t1 to the known type for more rigorous type checking
            if (type1.contains(" UNKNOWN ") && !type2.contains(" UNKNOWN ")) {
               unknownSyms.expectType(unknownSyms.exprNum(), type2);
               type1 = type2;
            }
            else if (!type1.contains(" UNKNOWN ") && type2.contains(" UNKNOWN ")) {
               unknownSyms.expectType(unknownSyms.exprNum(), type1);
            }
            // the last two unknowns need their expected types from the operation
            else if (type1.contains(" UNKNOWN ") && type2.contains(" UNKNOWN ")) {
               unknownSyms.expectType(unknownSyms.exprNum() - 1, null, next_token.lexeme);
               unknownSyms.expectType(unknownSyms.exprNum() , null, next_token.lexeme);
            }
         }
         else {
            (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
               incompatible_type_operation(next_token.lexeme, type1, type2);
         }

         // code generation
         if (next_token.lexeme.equals("=")) {
            code.write("eq");
         }
         else if (next_token.lexeme.equals(">")) {
            code.write("gt");
         }
         else {
            code.write("lt");
         }

         next_token = lexer.PeekNextToken();
      }
      if (isBoolExp) {
         return "boolean";
      }
      return type1;
   }

   //arithmetic expression
   private String arithExp() {
      String type1, type2;
      type1 = term();
      // 0 or more relational expressions with boolean operators
      Token next_token = lexer.PeekNextToken();
      while (next_token.lexeme.equals("+") || next_token.lexeme.equals("-")) {
         // consume the token
         lexer.GetNextToken();
         type2 = term();
         // check if types are compatible and swap if necessary
         if (Semantics.checkTypesValid(type1, type2, next_token.lexeme)){
            // swapping t1 to the known type for more rigorous type checking
            if (type1.contains(" UNKNOWN ") && !type2.contains(" UNKNOWN ")) {
               unknownSyms.expectType(unknownSyms.exprNum(), type2);
               type1 = type2;
            }
            else if (!type1.contains(" UNKNOWN ") && type2.contains(" UNKNOWN ")) {
               unknownSyms.expectType(unknownSyms.exprNum(), type1);
            }
            // the last two unknowns need their expected types from the operation
            else if (type1.contains(" UNKNOWN ") && type2.contains(" UNKNOWN ")) {
               unknownSyms.expectType(unknownSyms.exprNum() - 1, null, next_token.lexeme);
               unknownSyms.expectType(unknownSyms.exprNum() , null, next_token.lexeme);
            }
         }
         else {
            (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
               incompatible_type_operation(next_token.lexeme, type1, type2);
         }

         // code generation
         if (next_token.lexeme.equals("+")) {
            code.write("add");
         }
         else {
            code.write("sub");
         }

         next_token = lexer.PeekNextToken();
      }
      return type1;
   }

   private String term() {
      String type1, type2;
      type1 = factor();
      // 0 or more relational expressions with boolean operators
      Token next_token = lexer.PeekNextToken();
      while (next_token.lexeme.equals("*") || next_token.lexeme.equals("/")) {
         // consume the token
         lexer.GetNextToken();
         type2 = factor();
         // check if types are compatible and swap if necessary
         if (Semantics.checkTypesValid(type1, type2, next_token.lexeme)){
            // swapping t1 to the known type for more rigorous type checking
            if (type1.contains(" UNKNOWN ") && !type2.contains(" UNKNOWN ")) {
               unknownSyms.expectType(unknownSyms.exprNum(), type2);
               type1 = type2;
            }
            else if (!type1.contains(" UNKNOWN ") && type2.contains(" UNKNOWN ")) {
               unknownSyms.expectType(unknownSyms.exprNum(), type1);
            }
            // the last two unknowns need their expected types from the operation
            else if (type1.contains(" UNKNOWN ") && type2.contains(" UNKNOWN ")) {
               unknownSyms.expectType(unknownSyms.exprNum() - 1, null, next_token.lexeme);
               unknownSyms.expectType(unknownSyms.exprNum() , null, next_token.lexeme);
            }
         }
         else {
            (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
               incompatible_type_operation(next_token.lexeme, type1, type2);
         }

         // code generation
         if (next_token.lexeme.equals("*")) {
            code.write("call Math.multiply 2");
         }
         else {
            code.write("call Math.divide 2");
         }

         next_token = lexer.PeekNextToken();
      }
      return type1;
   }

   private String factor() {
      String type;
      Token next_token = lexer.PeekNextToken();
      if (next_token.lexeme.equals("-") || next_token.lexeme.equals("~")) {
         // consume token
         lexer.GetNextToken();
         type = operand();
         // check if types are compatible
         if (!Semantics.checkTypesValid(type, type, next_token.lexeme)) {
            (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
               incompatible_type_operation(next_token.lexeme, type, type);
         }
         if (type.contains(" UNKNOWN ")) {
            unknownSyms.expectType(unknownSyms.exprNum(), null, next_token.lexeme);
         }

         // code generation
         if (next_token.lexeme.equals("-")) {
            code.write("neg");
         }
         else {
            // boolean negation
            code.write("not");
         }

         return type;
      }
      return operand();
   }

   private String operand() {
      String type = null;
      Token next_token = lexer.GetNextToken();
      if (next_token.tokenType == Token.TokenTypes.integer) {
         type = "int";
         code.write("push constant " + next_token.lexeme);
      }
      else if (next_token.tokenType == Token.TokenTypes.string_literal) {
         type = "String";
         // code gen for building string using JACK library
         code.write("push constant " + String.valueOf(next_token.lexeme.length()));
         code.write("call String.new 1");
         for (char c : next_token.lexeme.toCharArray()) {
            code.write("push constant " + (int) c);
            code.write("call String.appendChar 2");
         }
      }
      else if (next_token.lexeme.equals("true")) {
         // true is 11111111 in binary, so push 0 and not it bitwise
         code.write("push constant 0");
         code.write("not");
         return "boolean";
      }
      else if (next_token.lexeme.equals("false")) {
         code.write("push constant 0");
         return "boolean";
      }
      else if (next_token.lexeme.equals("null")) {
         // push 0 for now, leave errors to run time exception or semantic analysis
         code.write("push constant 0");
         return "null";
      }
      else if (next_token.lexeme.equals("this")) {
         code.write("push pointer 0");
         return symTables.getClassName();
      }
      else if (next_token.lexeme.equals("(")) {
         type = expression();
         next_token = lexer.GetNextToken();
         if (next_token.lexeme.equals(")")) {
         }
         else {
            (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
               invalid_lexeme(")", "symbol", next_token.lexeme);
         }
      }
      else if (next_token.tokenType == Token.TokenTypes.id) {
         Token cur_token = next_token;
         // operand could just be an identifier or also a subroutine call
         next_token = lexer.PeekNextToken();
         if (next_token.lexeme.equals(".")) {
            // consume token
            lexer.GetNextToken();
            String id = identifier();
            next_token = lexer.PeekNextToken();
            // operand of form: class.id(expr_list)
            if (next_token.lexeme.equals("(")) {
               SymbolTable curSymTable = symTables.getClassSub(cur_token.lexeme, id);
               type = getTypeFromSymTable(curSymTable, cur_token.lexeme, id);

               // add called subroutine to sub table
               subTables.addTable(
                  cur_token.lexeme, id, lexer.getLineNum(),
                  symTables.getClassName()
               );
               lexer.GetNextToken();
               if (curSymTable != null) {
                  if (curSymTable.kind == SymbolTable.Kind._method) {
                     Symbol s = symTables.find(cur_token.lexeme);
                     if (s != null) {
                        code.write("push", s);
                     }
                  }
               }
               else {
                  code.write(String.format("do %s.%s %s", cur_token.lexeme, id, symTables.getClassName()));
               }
               // retrieve parameters
               exprList();
               subTables.goToParentTable();

               next_token = lexer.GetNextToken();
               if (next_token.lexeme.equals(")")) {
               }
               else {
                  (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
                     invalid_lexeme(")", "symbol", next_token.lexeme);
               }

               // at this point any arguments for a function call will have been
               // pushed onto the stack, so call the function
               code.write(String.format("call %s.%s %s", cur_token.lexeme, id, symTables.getClassName()));

            }
            // operand of form: class.id[some_integer_expr]
            else if (next_token.lexeme.equals("[")) {
               Symbol curSymbol = symTables.getClassVar(cur_token.lexeme, id);
               type = getTypeFromSymbol(curSymbol, cur_token.lexeme, id);

               // consume token
               lexer.GetNextToken();
               type = expression();
               if (!type.equals("int") && !type.equals("char")) {
                  (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
                     array_index(
                        cur_token.lexeme + "." + id + "[not_an_int]", type
                  );
               }
               next_token = lexer.GetNextToken();
               if (next_token.lexeme.equals("]")) {
               }
               else {
                  (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
                     invalid_lexeme("]", "symbol", next_token.lexeme);
               }

               // at this point the array index value should be pushed to the stack
               // need to add to start of array pointer to get new address
               // then retrieve the value at that address
               if (curSymbol != null) {
                  code.write("push", curSymbol);
                  // add the array index to the pointer to start of array
                  code.write("add");
                  // store location of array pointer in 'that' pointer mem. loc.
                  code.write("pop pointer 1");
                  // put value stored in the specific array index onto stack
                  code.write("push that 0");
               }
            }
            // operand of form: class.id
            else {
               Symbol curSymbol = symTables.getClassVar(cur_token.lexeme, id);
               type = getTypeFromSymbol(curSymbol, cur_token.lexeme, id);
            }
         }
         // i.e. no .child, just an expression list, so identifier(exprList)
         else if (next_token.lexeme.equals("(")) {
            SymbolTable curSymTable = symTables.getClassSub(symTables.getClassName(), cur_token.lexeme);
            type = getTypeFromSymTable(curSymTable, symTables.getClassName(), cur_token.lexeme);

            subTables.addTable(
               symTables.getClassName(), cur_token.lexeme,
               lexer.getLineNum(), symTables.getClassName()
            );
            // consume token
            lexer.GetNextToken();
            exprList();
            subTables.goToParentTable();

            next_token = lexer.GetNextToken();
            if (next_token.lexeme.equals(")")) {
            }
            else {
               (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
                  invalid_lexeme(")", "symbol", next_token.lexeme);
            }

            // at this point any arguments for a function call will have been
            // pushed onto the stack, so call the function
            code.write(String.format("call %s.%s %s", symTables.getClassName(),
                       cur_token.lexeme, symTables.getClassName()
            ));

         }
         // otherwise, could be an array , i.e. identifier[expression]
         else if (next_token.lexeme.equals("[")) {
            Symbol curSymbol = symTables.find(cur_token.lexeme);
            type = getTypeFromSymbol(curSymbol, cur_token.lexeme, "");

            if (type.contains(" UNKNOWN ")) {
               // this array has not been declared previously, raise error
               (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
                  not_declared(cur_token.lexeme);
            }
            // checking if this identifier is not an array in the symbol tables
            if (!type.equals("Array")) {
               (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
                  invalid_type(cur_token.lexeme, "Array");
            }
            // consume token
            lexer.GetNextToken();
            type = expression();
            if (!type.equals("int") && !type.equals("char")) {
               (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
                  array_index(cur_token.lexeme + "[not_an_int]", type);
            }
            next_token = lexer.GetNextToken();
            if (next_token.lexeme.equals("]")) {
            }
            else {
               (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
                  invalid_lexeme("]", "symbol", next_token.lexeme);
            }

            // at this point the array index value should be pushed to the stack
            // need to add to start of array pointer to get new address
            // then retrieve the value at that address
            if (curSymbol != null) {
               code.write("push", curSymbol);
               // add the array index to the pointer to start of array
               code.write("add");
               // store location of array pointer in 'that' pointer mem. loc.
               code.write("pop pointer 1");
               // put value stored in the specific array index onto stack
               code.write("push that 0");
            }
         }
         // just the identifier on its own - e.g. the 'y' in 'x = y + 5;'
         else {
            Symbol cur_symbol = symTables.find(cur_token.lexeme);
            // if the identifier has not been declared before
            if (cur_symbol == null) {
               (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
                  not_declared(cur_token.lexeme);
            }
            if (cur_symbol.has_value == false) {
               (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
                  not_initialised(cur_token.lexeme);
            }
            type = cur_symbol.type;
            code.write("push", cur_symbol);
         }
      }
      else {
         (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
            invalid_lexeme("", "operand", next_token.lexeme);
      }
      return type;
   }

   private String identifier() {
      Token next_token = lexer.GetNextToken();
      if (next_token.tokenType == Token.TokenTypes.id) {
      }
      else {
         (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
            invalid_lexeme("identifier", "", next_token.lexeme);
      }
      return next_token.lexeme;
   }

   private void zeroOrMoreStmnts() {
      Token next_token = lexer.GetNextToken();
      if (next_token.lexeme.equals("{")) {
      }
      else {
         (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
            invalid_lexeme("{", "symbol", next_token.lexeme);
      }

      next_token = lexer.PeekNextToken();
      // can have 0 or more statements in the curly braces
      while (next_token.lexeme.equals("var") || next_token.lexeme.equals("let") ||
            next_token.lexeme.equals("if") || next_token.lexeme.equals("while") ||
            next_token.lexeme.equals("do") || next_token.lexeme.equals("return"))
      {
         statement();
         next_token = lexer.PeekNextToken();
      }

      next_token = lexer.GetNextToken();
      if (next_token.lexeme.equals("}")) {
         // exit current scope
         symTables.goToParentTable();
      }
      else {
         (new Error(symTables.getCurTableInfo(), lexer.getLineNum())).
            invalid_lexeme("}", "symbol", next_token.lexeme);
      }
   }


   // following 2 methods are used to retrieve types of identifer operands -
   // could be unknown types at this point - in another file or later in the same file

   private String getTypeFromSymbol(Symbol curSymbol, String classRef, String id) {
      String type = null;
      if(curSymbol == null) {
         // i.e. encountered identifier on it's own (exists in current class),
         // rather than classRef.identifier
         if(id.equals("")) {
            // get the className from the current class
            id = classRef;
            classRef = symTables.getClassName();
         }
         type = " UNKNOWN " + String.valueOf(unknownSyms.size());
         unknownSyms.add(id, type, lexer.getLineNum(), classRef, symTables.getClassName());
      }
      else {
         type = curSymbol.type;
      }
      return type;
   }

   private String getTypeFromSymTable(SymbolTable curSymTable, String classRef, String id) {
      String type = null;
      if(curSymTable == null) {
         type = " UNKNOWN " + String.valueOf(unknownSyms.size());
         unknownSyms.add(id, type, lexer.getLineNum(), classRef, symTables.getClassName());
      }
      else {
         type = curSymTable.type;
      }
      return type;
   }
}
