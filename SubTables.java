import java.util.List;
import java.util.ArrayList;
// maintains a list of every called subroutine with relevant information

public class SubTables {
   public List<SubTable> table;
   private int cur_index;

   // constructor initialise empty list
   public SubTables() {
      table = new ArrayList<SubTable>();
   }

   // adds to the table of called subroutines - with relevant information
   public void addTable(String className, String name, int line_num, String file) {
      // first table has negative parent index as it has no parent
      int parent_index = -1;
      if(table.size() > 0) {
         parent_index = cur_index;
      }
      table.add(new SubTable(className, name, line_num, parent_index, file));
      // current table is the table we have just added - update the index
      cur_index = table.size() - 1;
   }

   public void goToParentTable() {
      cur_index = table.get(cur_index).parent_index;
      if (cur_index == -1) {
         cur_index = 0;
      }
   }

   // adds the type of the parameter to the current called subroutine table
   public void addArgType(String type) {
      table.get(cur_index).add(type);
   }

   // finds and retrieves a subroutine with a certain name
   public SubTable findSub(String name) {
      for (SubTable subTable : table) {
         if (subTable.name.equals(name)) {
            return subTable;
         }
      }
      return null;
   }

   // replaces any unknown parameter types in the called subroutines with
   // their now known types
   // this ,ethod is called after parsing each file in directory
   public SubTables replaceUnknownTypes(UnknownSymbolTable u_s_table) {
      for (SubTable subTable : table) {
         for (String type : subTable.args) {
            if (type.contains(" UNKNOWN ")) {
               // type is of form " UNKNOWN x", where x means the xth unknown
               // variable the parser has come across
               int exprNum = Integer.valueOf(type.split(" ")[2]);
               UnknownSymbol s = u_s_table.table.get(exprNum);
               // update unknown type with correct type
               type = s.type;
               // update reference to className
               subTable.className = s.className;
            }
         }
      }
      return this;
   }

   // prints the subtables for debugging
   public void print() {
      System.out.print("\n");
      for(SubTable t: table){
         t.print();
      }
   }
}
