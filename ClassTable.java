import java.util.List;
import java.util.ArrayList;
// a table for each class consisting of all the symbol tables in that class (.jack file)
public class ClassTable {
   public List<SymbolTable> table;
   private int cur_index;

   // constructor - initialise list and add the global scope table
   public ClassTable(String name) {
      table = new ArrayList<SymbolTable>();
      cur_index = 0;
      addTable(name, "", SymbolTable.Kind._class, -1);
   }

   // returns number of symbol tables currently stored
   public int size() {
      return table.size();
   }

   // gets current table
   public SymbolTable curTable() {
      return table.get(cur_index);
   }

   // adds a new symboltable to the lsit of ClassTable with metadata
   public void addTable(String name, String type, SymbolTable.Kind kind, int line_num) {
      // first table has negative parent index as it has no parent
      int parent_index = -1;
      if(size() > 0) {
         parent_index = cur_index;
      }
      table.add(new SymbolTable(name, type, kind, parent_index, line_num));
      // current table is the table we have just added - update the index
      cur_index = size() - 1;
   }

   public void goToParentTable() {
      cur_index = curTable().parent_index;
      if (cur_index == -1) {
         cur_index = 0;
      }
   }

   // removes last symboltable from list of symbol tables
   public void removeTable() {
      table.remove(size() - 1);
   }

   // gets the name of the class the JACK program is named after - first table name
   public String getClassName() {
      return table.get(0).name;
   }

   // add symbol to current symboltable
   public void addSymbol(String name, String type, Symbol.Kind kind, int line_num) {
      addSymbol(name, type, kind, false, line_num);
   }

   // add symbol to current symboltable
   public void addSymbol(String name, String type, Symbol.Kind kind,
                         Boolean has_value, int line_num) {
      curTable().add(name, type, kind, has_value, line_num);
   }

   // searches current scope for a symbol - used to check redeclaration error
   public Symbol findLocalScope(String name) {
      return curTable().find(name);
   }

   // searches all the tables backwards for a symbol given its name only
   public Symbol find(String name) {
      Symbol symbol;
      // iterating up the tables from most nested scope to global scope
      for(int i = cur_index; i >= 0; i = table.get(i).parent_index) {
         // utilising the find method of the SymbolTable class
         symbol = table.get(i).find(name);
         if(symbol != null) {
            return symbol;
         }
         if(table.get(i).kind == SymbolTable.Kind._class) {
             return null;
         }
      }
      return null;
   }

   // searches all the tables backwards for a symbol given its name and type
   public Symbol find(String name, String type) {
      Symbol symbol;
      // iterating up the tables from most nested scope to global scope
      for(int i = cur_index; i >= 0; i = table.get(i).parent_index) {
         // utilising the find method of the SymbolTable class
         symbol = table.get(i).find(name, type);
         if(symbol != null) {
            return symbol;
         }
         if(table.get(i).kind == SymbolTable.Kind._class) {
             return null;
         }
      }
      return null;
   }

   public void assignedValue(String name) {
      Symbol symbol = find(name);
      if(symbol != null) {
         symbol.has_value = true;
      }
   }

   // get the current subroutine type the current scope is in
   // e.g. an if statement encapsulated in an int method, will return int
   public String getCurSubType() {
      SymbolTable curTable;
      for(int i = cur_index; i >= 0; i = table.get(i).parent_index) {
         // utilising the find method of the SymbolTable class
         curTable = table.get(i);
         // i.e. the current symbol table is a constructor, method or function
         if(!curTable.type.equals("")) {
            return curTable.type;
         }
      }
      return null;
   }

   public String getCurTableInfo() {
      // removing the _ from _constructor for example
      return getTableInfo(curTable());
   }

   public String getTableInfo(SymbolTable t) {
      // removing the _ from _constructor for example
      return String.format("%s %s %s     in %s.jack",
                           t.kind.toString().substring(1), t.type, t.name,
                           getClassName()
      );
   }

   // used as a helper method for padding in print method
   private int curScope(SymbolTable t) {
      int scope = 0;
      for(int i = table.indexOf(t); i > 0; i = table.get(i).parent_index) {
         ++scope;
      }
      return scope;
   }

   // printing all the tables and their symbols
   public void print() {
      for(SymbolTable t : table) {
         // to work out the offset from the left when printing - larger offset represents more nested scopes
         int curScope = curScope(t);
         // printing the header and table infromation - i.e. which scope, method/function etc
         System.out.println();
         for(int i = 0; i <= 3 * curScope; ++i) {
            System.out.print(" ");
         }
         System.out.println(String.format("%-70s", "").replace(" ", "="));
         for(int i = 0; i <= 3 * curScope; ++i) {
            System.out.print(" ");
         }
         String table_header = getTableInfo(t);
         int padding = (int)((67 - table_header.length()) / 2) + table_header.length();
         System.out.println(String.format("%" + padding + "s", table_header));
         for(int i = 0; i <= 3 * curScope; ++i) {
            System.out.print(" ");
         }
         System.out.println(String.format("%-70s", "").replace(" ", "="));
         // printing the symbols in the table
         t.print(curScope);
      }
   }
}
