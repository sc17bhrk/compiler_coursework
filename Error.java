// Error class to display the potential errors and warnings that may be output
// the errors are self explanatory from their names and msgs

public class Error {
   private String info;
   private int line_num;

   // info and line number constructor
   public Error(String info, int line) {
      this.info = info;
      line_num = line;
   }

   // info only constructor
   public Error(String info) {
      this.info = info;
   }

   // line number only constructor
   public Error(int line) {
      this(null, line);
   }

   // empty constructor
   public Error() {
   }

   // ------LEXICAL ERRORS------

   // error functions to print information and then halt the program
   public void UnexpectedEOFException(char c) {
      String err = "Error.\n";
      if(c == '\"'){
         err += "Missing a closing speech mark for \" ";
      }
      else{
         err += "Multi-line comment not closed for /* ";
      }
      err += String.format("which starts on line %d\n", line_num);

      System.out.println(err);
      System.exit(0);
   }

   public void IllegalCharacterException(char c) {
      String err = "Error.\n";
      err += String.format("Character %c not allowed in JACK language\n", c);
      err += String.format("Error on line %d\n", line_num);

      System.out.println(err);
      System.exit(0);
   }


   // ------PARSER GRAMMATICAL ERRORS------

   public void invalid_lexeme(String ideal, String tokenType, String lexeme) {
      System.out.printf("ERROR\nThere should be a %s '%s' here", tokenType, ideal);
      System.out.printf(", close to '%s'\n", lexeme);
      System.out.printf("Line number %d\n\n", line_num);
      System.exit(0);
   }

   // ------SEMANTIC ANALYSIS ERRORS------

   public void constructor(String sub_name, String given_type, String className) {
      System.out.printf("WARNING\nInvalid construtor declaration\nshould be of type ");
      System.out.printf("'%s', not '%s' in", className, given_type);
      System.out.printf("\n   %s\n", info);
      System.out.printf("Line number %d\n\n", line_num);
      // System.exit(0);
   }

   public void redeclare(String var) {
      System.out.printf("ERROR\nThe variable '%s' has been declared twice in", var);
      System.out.printf("\n   %s\n", info);
      System.out.printf("Line number %d\n\n", line_num);
      // symTables.print();
      System.exit(0);
   }

   public void not_declared(String var) {
      System.out.printf("ERROR\nThe variable '%s' has not been declared before in", var);
      System.out.printf("\n   %s\n", info);
      System.out.printf("Line number %d\n\n", line_num);
      // symTables.print();
      System.exit(0);
   }

   public void not_initialised(String var) {
      System.out.printf("WARNING\nThe variable '%s' has not been assigned a value before use", var);
      System.out.printf("\n   %s\n", info);
      System.out.printf("Line number %d\n\n", line_num);
      // System.exit(0);
   }

   public void not_bool_expr(String type) {
      System.out.printf("WARNING\nThe condition should have a boolean expression");
      System.out.printf("\nrather than type '%s' in ", type);
      System.out.printf("\n   %s\n", info);
      System.out.printf("Line number %d\n\n", line_num);
      // symTables.print();
      // System.exit(0);
   }

   public void invalid_type(String var, String type) {
      System.out.printf("WARNING\nThe variable '%s' should be of type %s in ", var, type);
      System.out.printf("\n   %s\n", info);
      System.out.printf("Line number %d\n\n", line_num);
      // symTables.print();
      // System.exit(0);
   }

   public void incompatible_type_operation(String op, String t1, String t2) {
      System.out.printf("WARNING\nCannot compute operation '%s' between ", op);
      System.out.printf("incompatible types '%s' and '%s' in ",t1, t2);
      System.out.printf("\n   %s\n", info);
      System.out.printf("Line number %d\n\n", line_num);
      // symTables.print();
      // System.exit(0);
   }

   public void invalid_symbol_type(Symbol s) {
      System.out.printf("WARNING\nThe identifier '%s' has invalid type '%s' in \n", s.name, s.type);
      System.out.printf("%s\n", info);
      System.out.printf("Line number %d\n\n", line_num);
      System.exit(0);
   }

   public void array_index(String var, String typeAssigned) {
      System.out.printf("WARNING\nThe array '%s' index should be of integer value, not type '%s' in ", var, typeAssigned);
      System.out.printf("\n   %s\n", info);
      System.out.printf("Line number %d\n\n", line_num);
   }

   public void return_not_last_stmt() {
      System.out.printf("ERROR\nThe return statement should be the last statement in");
      System.out.printf("\n   %s\n", info);
      System.out.printf("Line number %d\n\n", line_num);
      System.exit(0);
   }

   public void return_should_be_void() {
      System.out.printf("ERROR\nThe return statement should not return an expression");
      System.out.printf("\nas the subroutine is of type void in");
      System.out.printf("\n   %s\n", info);
      System.out.printf("Line number %d\n\n", line_num);
      System.exit(0);
   }

   public void invalid_return_type(String sub_type, String ret_type) {
      System.out.printf("WARNING\nThe return statement should return expression of type");
      System.out.printf(" '%s' instead of type '%s'", sub_type, ret_type);
      System.out.printf("\nas the subroutine is of type '%s' in", sub_type);
      System.out.printf("\n   %s\n", info);
      System.out.printf("Line number %d\n\n", line_num);
      // System.exit(0);
   }

   public void return_should_be_this() {
      System.out.printf("WARNING\nThe return statement should be of the form 'return this;'");
      System.out.printf("\nas the subroutine is a constructor in");
      System.out.printf("\n   %s\n", info);
      System.out.printf("Line number %d\n\n", line_num);
      // System.exit(0);
   }

   public void unconditional_return() {
      System.out.printf("ERROR\nUnconditional return statement, ");
      System.out.printf("should be in a subroutine or an if statement, in");
      System.out.printf("\n   %s\n", info);
      System.out.printf("Line number %d\n\n", line_num);
      System.exit(0);
   }

   public void no_return() {
      System.out.printf("ERROR\n    %s\ndoes not have return statement\n",
         info
      );
      System.out.printf("Line number %d\n\n", line_num);
      System.exit(0);
   }

   public void not_enough_args(SubTable subTable) {
      System.out.printf("ERROR\nNot enough arguments have been sent to ");
      System.out.printf("   %s\n", info);
      System.out.printf("in %s.%s\n", subTable.className, subTable.name);
      System.out.printf("Line number %d\n\n", line_num);
      System.exit(0);
   }

   public void too_many_args(SubTable subTable) {
      System.out.printf("ERROR\nToo many arguments have been sent to ");
      System.out.printf("   %s\n", info);
      System.out.printf("in %s.%s\n", subTable.className, subTable.name);
      System.out.printf("Line number %d\n\n", line_num);
      System.exit(0);
   }

   public void invalid_param_type(SubTable subTable, String argType, String expectedType, int i) {
      System.out.printf("WARNING\nIn %s\nThe parameter(no.%d) has invalid type '%s' ",
         info, i + 1, argType
      );
      System.out.printf("but should be of type '%s' in\n", expectedType);
      System.out.printf("   %s.%s(", subTable.className, subTable.name);
      for (int k = 0; k < subTable.args.size() ; ++k) {
         if (k == subTable.args.size() - 1) {
            System.out.printf("no.%d)\n", k + 1);
         }
         else {
            System.out.printf("no.%d, ", k + 1);
         }
      }
      System.out.printf("Line number %d     in %s.jack\n\n", line_num, subTable.file);
   }

   public void unknown_symbol_not_exist(UnknownSymbol s) {
      System.out.printf("ERROR\n   %s in %s.jack does not exist\n",
         s.name, s.className
      );
      System.out.printf("Line number %d in %s\n\n", s.line_num, s.file);
      System.exit(0);
   }

   public void not_found_unknown_expr(UnknownSymbol s, String type) {
      System.out.printf("WARNING\n   %s in %s.jack has incorrect type\n",
         s.name, s.className
      );
      System.out.printf("Expected type %s, but %s.%s actually has type %s\n",
         s.expectedType, s.className, s.name, type
      );
      System.out.printf("Line number %d in %s\n\n", s.line_num, s.file);
      // System.exit(0);
   }

}
