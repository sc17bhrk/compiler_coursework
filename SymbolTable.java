import java.util.List;
import java.util.ArrayList;

// represents a table of symbols with some meta data
public class SymbolTable {
   public enum Kind {_class, _constructor, _method, _function, _if, _while}
   public List<Symbol> table;
   public int sta_arg_offset, field_loc_offset, parent_index, line_num;
   public String name, type;
   public Kind kind;
   public boolean has_return;

   // constructor - initialise list of symbols
   public SymbolTable() {
      table = new ArrayList<Symbol>();
      sta_arg_offset = 0;
      field_loc_offset = 0;
      // set to false, until a return statement is encountered
      // to ensure every const/method/func has a return statement
      has_return = false;
   }

   // constructor - initialise list of symbols with metadata regarding the table
   public SymbolTable(String _name, String _type, SymbolTable.Kind _kind,
                        int p_index, int _line_num) {
      this();
      name = _name;
      type = _type;
      kind = _kind;
      parent_index = p_index;
      line_num = _line_num;
   }

   // adds a symbol to the list of symbols
   public void add(String _name, String _type, Symbol.Kind _kind,
                   Boolean _has_value, int _line_num) {
      if (_kind == Symbol.Kind._static || _kind == Symbol.Kind._argument) {
         table.add(new Symbol(_name, _type, _kind, sta_arg_offset++, _has_value, _line_num));
      }
      else {
         table.add(new Symbol(_name, _type, _kind, field_loc_offset++, _has_value, _line_num));
      }
      // for pushing arguments, the first argument should have 0 offset,
      // ignore "this" argument in construuctors, only used in checking validity
      if (_name.equals("this") && this.kind == Kind._constructor) {
         sta_arg_offset--;
      }
   }

   // return number of arguments expected
   public int getNumArgs() {
      int numArgs = 0;
      for (Symbol s : table) {
         if (s.kind == Symbol.Kind._argument || s.kind == Symbol.Kind._static) {
            ++numArgs;
         }
      }
      return numArgs;
   }

// return number of local variables used
   public int getNumLocalVars() {
      int numLocalVars = 0;
      for (Symbol s : table) {
         if (s.kind == Symbol.Kind._field || s.kind == Symbol.Kind._var) {
            ++numLocalVars;
         }
      }
      return numLocalVars;
   }

   // finds a symbol with a matching name
   public Symbol find(String _name) {
      for(Symbol symbol : table) {
         if(symbol.name.equals(_name)) {
            return symbol;
         }
      }
      return null;
   }

   // finds a symbol with a matching name and type
   public Symbol find(String _name, String _type) {
      for(Symbol symbol : table) {
         if(symbol.name.equals(_name) && symbol.type.equals(_type)) {
            return symbol;
         }
      }
      return null;
   }

   // prints information regarding the symbol table
   public void print(int index) {
      if(table.size() == 0) {
         for(int i = 0; i <= 3 * index; ++i) {
            System.out.print(" ");
         }
         String msg = "No local variables";
         int padding = (int)((67 - msg.length()) / 2) + msg.length();
         System.out.println(String.format("%" + padding + "s", msg));
         return;
      }

      for(int i = 0; i <= 3 * index; ++i) {
         System.out.print(" ");
      }
      System.out.println(String.format("%-15s %-15s %-15s %-10s %-10s", "name", "type", "kind", "offset", "has_value"));

      for(Symbol s : table) {
         for(int i = 0; i <= 3 * index; ++i) {
            System.out.print(" ");
         }
         System.out.println(String.format("%-15s %-15s %-15s %-10s %-10s", s.name, s.type, s.kind, s.offset, s.has_value));
      }
   }
}
