// holds information regarding the symbol

public class Symbol {
   public enum Kind {_static, _field, _var, _argument}
   public String name;
   public String type;
   public Kind kind;
   public int offset;
   public int line_num;
   public Boolean has_value;

   public Symbol(String _name, String _type, Kind _kind,
                 int _offset, boolean _has_value, int _line_num) {
      name = _name;
      type = _type;
      kind = _kind;
      offset = _offset;
      has_value = _has_value;
      line_num = _line_num;
   }
}
