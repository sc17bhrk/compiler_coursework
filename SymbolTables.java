import java.util.ArrayList;
// a table to hold all the class tables with many methods to retrieve information

public class SymbolTables {
   private ArrayList<ClassTable> table;

   // constructor - initialise list and add the JACK library classes
   public SymbolTables() {
      table = new ArrayList<ClassTable>();

      addMathClass();
      addArrayClass();
      addMemoryClass();
      addScreenClass();
      addKeyboardClass();
      addOutputClass();
      addStringClass();
      addSysClass();
   }

   // getter for the table
   public ArrayList<ClassTable> getTable() {
      return table;
   }

   // add a new class to the table for a new .jack file
   public void addClass(String name) {
      table.add(new ClassTable(name));
   }

   public int size() {
      return table.size();
   }

   public ClassTable curClass() {
      return table.get(size() - 1);
   }

   public SymbolTable curTable() {
      return curClass().curTable();
   }

   // adds a new SymbolTable to the list of symboltables with metadata in current class
   public void addTable(String name, String type, SymbolTable.Kind kind, int line_num) {
      curClass().addTable(name, type, kind, line_num);
      // adding the implicit "this" argument symbol to symbol table
      // - functions do not have "this"
      if (kind != SymbolTable.Kind._function && !type.equals("")) {
         addSymbol("this", getClassName(), Symbol.Kind._argument, true, line_num);
      }
   }

   public void goToParentTable() {
      curClass().goToParentTable();
   }

   // gets the name of the class the JACK program is named after - first table name
   public String getClassName() {
      return curClass().getClassName();
   }

   // add symbol to current symboltable
   public void addSymbol(String name, String type, Symbol.Kind kind, int line_num) {
      addSymbol(name, type, kind, false, line_num);
   }

   // add symbol to current symboltable
   public void addSymbol(String name, String type, Symbol.Kind kind,
                         Boolean has_value, int line_num) {
      curClass().addSymbol(name, type, kind, has_value, line_num);
   }

   // searches current scope for a symbol - used to check redeclaration error
   public Symbol findLocalScope(String name) {
      return curClass().findLocalScope(name);
   }

   // searches all the tables backwards for a symbol given its name only
   public Symbol find(String name) {
      return curClass().find(name);
   }

   // searches all the tables backwards for a symbol given its name and type
   public Symbol find(String name, String type) {
      return curClass().find(name, type);
   }

   public void assignedValue(String name) {
      curClass().assignedValue(name);
   }

   // showing the current const/func/method has a return value
   public void hasReturn() {
      curClass().curTable().has_return = true;
   }

   // get the current subroutine type the current scope is in
   // e.g. an if statement encapsulated in an int method, will return int
   public String getCurSubType() {
      return curClass().getCurSubType();
   }

   // gets the symbol id from a certain class
   // e.g. when parser comes across an operand like class.id
   public Symbol getClassVar(String classRef, String id) {
      // checking current class static/field variables
      if(classRef.equals("this")) {
         return curClass().table.get(0).find(id);
      }
      // checking if the classRef is a variable of type class elsewhere
      Symbol curSymbol = find(classRef);
      if((curSymbol) != null) {
         classRef = curSymbol.type;
      }
      // otherwise assume the classRef is the actual class name
      // checking external class static/field variables
      ClassTable cTable = getClassTable(classRef);
      if (cTable == null) {
         return null;
      }
      return cTable.table.get(0).find(id);
   }

   // gets the symbolTable from a certain class
   // e.g. when parser comes across an operand like class.id(expr_list)
   public SymbolTable getClassSub(String classRef, String id) {
      // checking current class static/field variables
      if(classRef.equals("this")) {
         return getSymbolTable(curClass(), id);
      }
      // checking if the classRef is a variable of type class elsewhere
      Symbol curSymbol = find(classRef);
      if((curSymbol) != null) {
         classRef = curSymbol.type;
      }
      // otherwise assume the classRef is the actual class name
      // checking external class static/field variables
      ClassTable cTable = getClassTable(classRef);
      if (cTable == null) {
         return null;
      }
      return getSymbolTable(cTable, id);
   }

   public String getClassRef(String classRef, String sub_name, String file) {
      if (classRef.equals("this")) {
         return file;
      }
      ClassTable cTable = getClassTable(classRef);
      if (cTable != null) {
         return classRef;
      }
      cTable = getClassTable(file);
      if (cTable != null) {
         // if the classname is actually an identifier which is a reference to another class
         Symbol sym = searchClass(classRef, cTable);
         if (sym != null) {
            return sym.type;
         }
      }
      return null;
   }

   public Symbol searchClass(String name, ClassTable cTable) {
      for (SymbolTable symTable : cTable.table) {
         Symbol s = symTable.find(name);
         if (s != null) {
            return s;
         }
      }
      return null;
   }

   public ClassTable getClassTable(String name) {
      for (ClassTable cTable : table) {
         if (cTable.getClassName().equals(name)) {
            return cTable;
         }
      }
      return null;
   }

   public SymbolTable getSymbolTable(ClassTable cTable, String name) {
      for (SymbolTable symTable : cTable.table) {
         if (symTable.name.equals(name)) {
            return symTable;
         }
      }
      return null;
   }
   public String getCurTableInfo() {
      // removing the _ from _constructor for example
      return curClass().getCurTableInfo();
   }

   // printing all the tables for each class and their symbols
   public void print() {
      for(ClassTable t : table) {
         t.print();
         System.out.println("\n");
      }
   }


   // add the classes for standard JACK libraries

   // add Math class and its function symbol tables
   private void addMathClass() {
      addClass("Math");

      addTable("abs", "int", SymbolTable.Kind._function,-1);
      addSymbol("x", "int", Symbol.Kind._argument, true, -1);
      goToParentTable();

      addTable("multiply", "int", SymbolTable.Kind._function,-1);
      addSymbol("x", "int", Symbol.Kind._argument, true, -1);
      addSymbol("y", "int", Symbol.Kind._argument, true, -1);
      goToParentTable();

      addTable("divide", "int", SymbolTable.Kind._function,-1);
      addSymbol("x", "int", Symbol.Kind._argument, true, -1);
      addSymbol("y", "int", Symbol.Kind._argument, true, -1);
      goToParentTable();

      addTable("min", "int", SymbolTable.Kind._function,-1);
      addSymbol("x", "int", Symbol.Kind._argument, true, -1);
      addSymbol("y", "int", Symbol.Kind._argument, true, -1);
      goToParentTable();

      addTable("max", "int", SymbolTable.Kind._function,-1);
      addSymbol("x", "int", Symbol.Kind._argument, true, -1);
      addSymbol("y", "int", Symbol.Kind._argument, true, -1);
      goToParentTable();

      addTable("sqrt", "int", SymbolTable.Kind._function,-1);
      addSymbol("x", "int", Symbol.Kind._argument, true, -1);
      goToParentTable();
   }

   // add Array class and its function symbol tables
   private void addArrayClass() {
      addClass("Array");

      addTable("new", "Array", SymbolTable.Kind._function,-1);
      addSymbol("size", "int", Symbol.Kind._argument, true, -1);
      goToParentTable();

      addTable("dispose", "void", SymbolTable.Kind._method,-1);
      goToParentTable();
   }

   // add Memory class and its function symbol tables
   private void addMemoryClass() {
      addClass("Memory");

      addTable("peek", "int", SymbolTable.Kind._function,-1);
      addSymbol("address", "int", Symbol.Kind._argument, true, -1);
      goToParentTable();

      addTable("poke", "void", SymbolTable.Kind._function,-1);
      addSymbol("address", "int", Symbol.Kind._argument, true, -1);
      addSymbol("value", "int", Symbol.Kind._argument, true, -1);
      goToParentTable();

      addTable("alloc", "Array", SymbolTable.Kind._function,-1);
      addSymbol("size", "int", Symbol.Kind._argument, true, -1);
      goToParentTable();

      addTable("deAlloc", "void", SymbolTable.Kind._function,-1);
      addSymbol("o", "Array", Symbol.Kind._argument, true, -1);
      goToParentTable();
   }

   // add Screen class and its function symbol tables
   private void addScreenClass() {
      addClass("Screen");

      addTable("clearScreen", "void", SymbolTable.Kind._function,-1);
      goToParentTable();

      addTable("setColor", "void", SymbolTable.Kind._function,-1);
      addSymbol("b", "boolean", Symbol.Kind._argument, true, -1);
      goToParentTable();

      addTable("drawPixel", "void", SymbolTable.Kind._function,-1);
      addSymbol("x", "int", Symbol.Kind._argument, true, -1);
      addSymbol("y", "int", Symbol.Kind._argument, true, -1);
      goToParentTable();

      addTable("drawLine", "void", SymbolTable.Kind._function,-1);
      addSymbol("x1", "int", Symbol.Kind._argument, true, -1);
      addSymbol("y1", "int", Symbol.Kind._argument, true, -1);
      addSymbol("x2", "int", Symbol.Kind._argument, true, -1);
      addSymbol("y2", "int", Symbol.Kind._argument, true, -1);
      goToParentTable();

      addTable("drawRectangle", "void", SymbolTable.Kind._function,-1);
      addSymbol("x1", "int", Symbol.Kind._argument, true, -1);
      addSymbol("y1", "int", Symbol.Kind._argument, true, -1);
      addSymbol("x2", "int", Symbol.Kind._argument, true, -1);
      addSymbol("y2", "int", Symbol.Kind._argument, true, -1);
      goToParentTable();

      addTable("drawCircle", "void", SymbolTable.Kind._function,-1);
      addSymbol("x", "int", Symbol.Kind._argument, true, -1);
      addSymbol("y", "int", Symbol.Kind._argument, true, -1);
      addSymbol("r", "int", Symbol.Kind._argument, true, -1);
      goToParentTable();
   }

   // add Keyboard class and its function symbol tables
   private void addKeyboardClass() {
      addClass("Keyboard");

      addTable("keyPressed", "char", SymbolTable.Kind._function,-1);
      goToParentTable();

      addTable("readChar", "char", SymbolTable.Kind._function,-1);
      goToParentTable();

      addTable("readLine", "String", SymbolTable.Kind._function,-1);
      addSymbol("message", "String", Symbol.Kind._argument, true, -1);
      goToParentTable();

      addTable("readInt", "int", SymbolTable.Kind._function,-1);
      addSymbol("message", "String", Symbol.Kind._argument, true, -1);
      goToParentTable();
   }

   // add Output class and its function symbol tables
   private void addOutputClass() {
      addClass("Output");

      addTable("init", "void", SymbolTable.Kind._function,-1);
      goToParentTable();

      addTable("moveCursor", "void", SymbolTable.Kind._function,-1);
      addSymbol("i", "int", Symbol.Kind._argument, true, -1);
      addSymbol("j", "int", Symbol.Kind._argument, true, -1);
      goToParentTable();

      addTable("printChar", "void", SymbolTable.Kind._function,-1);
      addSymbol("c", "char", Symbol.Kind._argument, true, -1);
      goToParentTable();

      addTable("printString", "void", SymbolTable.Kind._function,-1);
      addSymbol("str", "String", Symbol.Kind._argument, true, -1);
      goToParentTable();

      addTable("printInt", "void", SymbolTable.Kind._function,-1);
      addSymbol("i", "int", Symbol.Kind._argument, true, -1);
      goToParentTable();

      addTable("println", "void", SymbolTable.Kind._function,-1);
      goToParentTable();

      addTable("backSpace", "void", SymbolTable.Kind._function,-1);
      goToParentTable();
   }

   // add String class and its function symbol tables
   private void addStringClass() {
      addClass("String");

      addTable("new", "String", SymbolTable.Kind._constructor,-1);
      addSymbol("maxLength", "int", Symbol.Kind._argument, true, -1);
      goToParentTable();

      addTable("dispose", "void", SymbolTable.Kind._method,-1);
      goToParentTable();

      addTable("length", "int", SymbolTable.Kind._method,-1);
      goToParentTable();

      addTable("charAt", "char", SymbolTable.Kind._method,-1);
      addSymbol("j", "int", Symbol.Kind._argument, true, -1);
      goToParentTable();

      addTable("setCharAt", "void", SymbolTable.Kind._method,-1);
      addSymbol("j", "int", Symbol.Kind._argument, true, -1);
      addSymbol("c", "char", Symbol.Kind._argument, true, -1);
      goToParentTable();

      addTable("appendChar", "String", SymbolTable.Kind._method,-1);
      addSymbol("c", "char", Symbol.Kind._argument, true, -1);
      goToParentTable();

      addTable("eraseLastChar", "void", SymbolTable.Kind._method,-1);
      goToParentTable();

      addTable("intValue", "int", SymbolTable.Kind._method,-1);
      goToParentTable();

      addTable("setInt", "void", SymbolTable.Kind._method,-1);
      addSymbol("number", "int", Symbol.Kind._argument, true, -1);
      goToParentTable();

      addTable("newLine", "char", SymbolTable.Kind._function,-1);
      goToParentTable();

      addTable("backSpace", "char", SymbolTable.Kind._function,-1);
      goToParentTable();

      addTable("doubleQuote", "char", SymbolTable.Kind._function,-1);
      goToParentTable();
   }

   // add Sys class and its function symbol tables
   private void addSysClass() {
      addClass("Sys");

      addTable("halt", "void", SymbolTable.Kind._function,-1);
      goToParentTable();

      addTable("error", "void", SymbolTable.Kind._function,-1);
      addSymbol("errorCode", "int", Symbol.Kind._argument, true, -1);
      goToParentTable();

      addTable("wait", "void", SymbolTable.Kind._function,-1);
      addSymbol("duration", "int", Symbol.Kind._argument, true, -1);
      goToParentTable();
   }
}
