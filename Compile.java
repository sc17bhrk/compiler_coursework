import java.io.File;

// Class to compile - just creates parser object, begins parsing/compilation process
public class Compile {
   public static void main(String[] args) {
      //checks whether the first argument is a directory, ceasing execution otherwise
      try {
         File file = new File(args[0]);
         if (!file.isDirectory()){
            System.out.println(args[0] + " is not a valid directory");
            return;
         }
         // run the parser/compiler on the directory
         Parser prog = new Parser(file);
         prog.run();
      }
      catch(NullPointerException ex) {
         System.out.println(args[0] + " is not a valid directory");
         return;
      }
   }
}
