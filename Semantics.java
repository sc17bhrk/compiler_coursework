import java.util.ArrayList;
// a class to handle semantic analysis

public class Semantics {
   private SymbolTables symTables;
   private ArrayList<ClassTable> table;

   // empty constructor
   public Semantics() {}

   // constructor which uses the symbol table
   public Semantics(SymbolTables symTables) {
      setSymTables(symTables);
   }

   public void setSymTables(SymbolTables symTables) {
      this.symTables = symTables;
      table = symTables.getTable();
   }

   // check if two types are valid for type conversion/type checking
   public static boolean checkTypesValid(String t1, String t2, String op) {
      if(t1 == null || t2 == null) {
         return true;
      }
      // this type is unknown so assume the types are equal for now
      if(t1.contains(" UNKNOWN ") && t2.contains(" UNKNOWN ")) {
         return true;
      }
      // array types can take any value - can assume their index values are valid types
      // leave errors to runtime exception
      if(t1.equals("Array") && t2.equals("Array")) {
         return true;
      } else if(t1.equals("Array") && t2.contains(" UNKNOWN ")) {
         return true;
      } else if(t1.contains(" UNKNOWN ") && t2.equals("Array")) {
         return true;
      }
      // char can be converted to int
      if(t1.equals("char")) {
         t1 = "int";
      }
      if(t2.equals("char")) {
         t2 = "int";
      }
      // array types can take any value so are compatible with other types
      if(t1.equals("Array") || t1.contains(" UNKNOWN ")) {
         t1 = t2;
      } else if(t2.equals("Array") || t2.contains(" UNKNOWN ")) {
         t2 = t1;
      }
      // at this point, t1 and t2 are not Array types or UNKNOWN types

      // String types can be a String or null
      if(op.equals("=") && t1.equals("String")) {
         return (t1.equals(t2) || t2.equals("null"));
      }

      // if types aren't the same there is a problem
      if(!t1.equals(t2)) {
         return false;
      }

      // considering the required types based on the operation
      if(op.equals("&") || op.equals("|")) {
         return (t1.equals("boolean") || t1.equals("int"));
      }
      else if(op.equals(">") || op.equals("<")) {
         return t1.equals("int");
      }
      else if(op.equals("+") || op.equals("-")) {
         return t1.equals("int");
      }
      else if(op.equals("*") || op.equals("/")) {
         return t1.equals("int");
      }
      else if(op.equals("-")) {
         return t1.equals("int");
      }
      else if(op.equals("~")) {
         return t1.equals("boolean");
      }

      return true;
   }

   // checking that the identifier types in every symbol table is valid,
   // i.e. type is an int, char, boolean,
   // or an existing Class instance (JACK library / user defined)
   public void checkVarTypesValid() {
      // start for index 8 to ignore the JACK library classes
      for(int i = 8; i < symTables.size(); ++i) {
         for(SymbolTable symTable: table.get(i).table) {
            for(Symbol s: symTable.table) {
               if(!s.type.equals("int") && !s.type.equals("char") && !s.type.equals("boolean")) {
                  if (checkTypeEqual(s) == false) {
                     (new Error(table.get(i).getTableInfo(symTable), s.line_num)).
                     invalid_symbol_type(s);
                  }
               }
            }
         }
      }
   }

   // helper method used for above method
   private boolean checkTypeEqual(Symbol s) {
      for(ClassTable t: table) {
         if (s.type.equals(t.getClassName())) {
            return true;
         }
      }
      return false;
   }

   // check that each constructor/method/function contains a return statement
   public void checkRetStmtsExist() {
      for(int i = 8; i < symTables.size(); ++i) {
         for(SymbolTable symTable: table.get(i).table) {
            // checking if there are any constructors/methods/funcs without return stmnts
            if(!symTable.type.equals("") && symTable.has_return == false) {
               // check if there are if-else stmnts in the body which have return stmnts
               if(!checkNestedIfElseHasRetStmts(table.get(i), symTable)) {
                  (new Error(table.get(i).getTableInfo(symTable), symTable.line_num)).
                  no_return();
               }
            }
         }
      }
   }

   // helper method used for above method
   private boolean checkNestedIfElseHasRetStmts(ClassTable cTable, SymbolTable symTable) {
      for(int i = 0; i < cTable.size() - 1; ++i) {
         SymbolTable ifTable = cTable.table.get(i);
         SymbolTable elseTable = cTable.table.get(i + 1);
         // if there is an if-else statement nested in the table symTable,
         // both of which have return statements, there is a return
         if(ifTable.kind == SymbolTable.Kind._if &&
            elseTable.kind == SymbolTable.Kind._if &&
            ifTable.name.equals("statement") &&
            elseTable.name.equals("else statement") &&
            ifTable.parent_index == cTable.table.indexOf(symTable) &&
            elseTable.parent_index == cTable.table.indexOf(symTable) &&
            ifTable.has_return == true && elseTable.has_return == true
         ) {
            return true;
         }
      }
      return false;
   }

   // retrieve unknown types using the classname and sub routine names
   public UnknownSymbolTable getUnknownExprTypes(UnknownSymbolTable u_s_table) {
      for(UnknownSymbol s: u_s_table.table) {
         // checking if the className is a variable of type class elsewhere
         // e.g. in PongGame, bat.getLeft(), bat is an instance of class Bat declared earlier
         Symbol sym = null;
         ClassTable cTable = symTables.getClassTable(s.file);
         if (cTable != null) {
            sym = symTables.searchClass(s.className, cTable);
         }
         if(sym != null) {
            s.className = sym.type;
         }

         cTable = symTables.getClassTable(s.className);
         sym = cTable.find(s.name);
         if(sym == null) {
            // need to check if it was a subroutine i.e. a symbolTable
            SymbolTable sTable = getUnknownSub(cTable, s);
            if(sTable == null) {
               (new Error()).unknown_symbol_not_exist(s);
            }
            else if(checkTypesValid(sTable.type, s.expectedType, "")) {
               s.type = sTable.type;
            }
            else {
               (new Error()).not_found_unknown_expr(s, sTable.type);
               // return;
            }
         }
         else if(checkTypesValid(sym.type, s.expectedType, "")) {
            s.type = sym.type;
         }
         else {
            (new Error()).not_found_unknown_expr(s, sym.type);
            // return;
         }
      }
      return u_s_table;
   }

   // helper method for above method - find symbolTable for unknown symbol (subroutine)
   private SymbolTable getUnknownSub(ClassTable cTable, UnknownSymbol s) {
      for(SymbolTable sTable: cTable.table) {
         if(sTable.name.equals(s.name)) {
            return sTable;
         }
      }
      return null;
   }

   // replace the unknown called subroutine parameter types using the now 
   // known types
   public SubTables replaceUnknownTypes(SubTables subTables, UnknownSymbolTable u_s_table) {
      for (SubTable subTable : subTables.table) {
         ClassTable cTable = symTables.getClassTable(subTable.file);
         if (cTable != null) {
            // if the classname is actually an identifier which is a reference to another class
            Symbol sym = symTables.searchClass(subTable.className, cTable);
            if (sym != null) {
               subTable.className = sym.type;
            }
         }
         // for (int i = 0; i < subTable.args.size(); ++i) {
         //    String type = subTable.args.get(i);
         for (String type : subTable.args) {
            if (type.contains(" UNKNOWN ")) {
               // type is of form " UNKNOWN x", where x means the xth unknown
               // variable the parser has come across
               int exprNum = Integer.valueOf(type.split(" ")[2]);
               UnknownSymbol s = u_s_table.table.get(exprNum);
               // update unknown type with correct type
               type = s.type;
            }
         }
      }
      return subTables;
   }

   // checking that a called subroutine and its arguments (in subTable)
   // match the expected type and number of parameters
   public void checkSubArgsMatch(SubTables subTables) {
      for (SubTable subTable : subTables.table) {
         ClassTable cTable = symTables.getClassTable(subTable.className);
         SymbolTable symTable = symTables.getSymbolTable(cTable, subTable.name);
         int i = 0;
         int j = i;
         // if the subroutine is a method or constructor, we must ignore the
         // the first implicit 'this' argument when comparing
         if (symTable.kind != SymbolTable.Kind._function) {
            j = i + 1;
         }
         for ( ; i < subTable.args.size(); ++i, ++j) {
            if (i >= symTable.table.size()) {
               (new Error(cTable.getTableInfo(symTable), subTable.line_num)).
               too_many_args(subTable);
            }
            String argType = subTable.args.get(i);
            String expectedType = symTable.table.get(j).type;
            if (!checkTypesValid(argType, expectedType, " ")) {
               (new Error(cTable.getTableInfo(symTable), subTable.line_num)).
               invalid_param_type(subTable, argType, expectedType, i);
            }
         }
         // at this point all the arguments sent were valid
         // need to check if there were more arguments expected
         // i.e. if the next symbol in the symbol table is an argument
         if (j < symTable.table.size()) {
            if (symTable.table.get(j).kind == Symbol.Kind._argument) {
               (new Error(cTable.getTableInfo(symTable), subTable.line_num)).
               not_enough_args(subTable);
            }
         }
      }
   }
}
