import java.util.List;
import java.util.ArrayList;
// maintains a list of the types of parameters sent to this called subroutine
// alond with other information

public class SubTable {
   // List of strings which contain the type
   public List<String> args;
   public String className;
   public String name;
   public int line_num;
   public int parent_index;
   public String file;

   // constructor - initialise list of symbols
   public SubTable() {
      args = new ArrayList<String>();
   }

   // constructor - initialise list of symbols with metadata regarding the args
   public SubTable(String className, String name, int line_num, int parent_index, String file) {
      this();
      this.className = className;
      this.name = name;
      this.line_num = line_num;
      this.parent_index = parent_index;
      this.file = file;
   }

   // adds an argument type to the list of types
   public void add(String type) {
      args.add(type);
   }

   // // prints information regarding the table
   public void print() {
      System.out.println(String.format("%-70s", "").replace(" ", "="));
      System.out.println(String.format("%-20s %-15s %-15s %-15s",
         " ", className, name, line_num)
         );
      System.out.println(String.format("%-70s", "").replace(" ", "="));
      if(args.size() == 0) {
         String msg = "No local variables";
         int padding = (int)((67 - msg.length()) / 2) + msg.length();
         System.out.println(String.format("%" + padding + "s", msg));
         return;
      }

      System.out.print(String.format("%10s %-5s ", " ", "type:"));

      for(String type : args) {
         System.out.print(String.format("%-10s", type));
      }
      System.out.print("\n\n");
   }
}
