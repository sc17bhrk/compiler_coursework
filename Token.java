// represents a token for the lexer and parser

public class Token{
   public enum TokenTypes{keyword, id, integer, string_literal, symbol, eof}
   public String lexeme;
   public TokenTypes tokenType;

   public Token(){
      lexeme = "";
   }

   @Override
   public String toString(){
      return String.format("<%s, %s>", lexeme, tokenType);
   }
}
