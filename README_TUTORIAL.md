Written instructions on how to run the compiler can be found in 
section 7 of the [Compiler Report - README](Compiler_Report_-_README.pdf) document.


A video tutorial has also been made for clarity.

However, since Gitlab restricts file sizes to a maximum of 10MB,
the video has been archived using B1 Archiver.

The video can be extracted by clicking on the [JACK Compiler Tutorial](JACK Compiler Tutorial/) directory
in the repository and downloading the directory (as a zip file from the top right).

Extract the directory and then extract the video
from the 26 split parts using [B1 Free Archiver](https://b1.org).