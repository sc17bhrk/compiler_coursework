import java.util.List;
import java.util.ArrayList;
// a class to hold a table of unknown symbols (subroutines)

public class UnknownSymbolTable {
   public List<UnknownSymbol> table;
   public int offset;

   // constructor - initialise list of symbols
   public UnknownSymbolTable() {
      table = new ArrayList<UnknownSymbol>();
      offset = 0;
   }

   public int size() {
      return table.size();
   }

   // retrieve which unknownth symbol we have now
   public int exprNum() {
      return size() - 1;
   }

   // adds a symbol to the list of symbols
   public void add(String name, String type, int line_num, String className, String file) {
      table.add(new UnknownSymbol(name, type, offset++, line_num, className, file));
   }

   // finds an unknown symbol with a certain name
   public UnknownSymbol find(String name) {
      for (UnknownSymbol s : table) {
         if (s.name.equals(name)) {
            return s;
         }
      }
      return null;
   }

   // assigns the expected type a certain unknown symbol given the parameters
   public void expectType(int exprNumber, String type) {
      table.get(exprNumber).expectedType = type;
   }

   // determine which type to expect based on the operation between the types
   public void expectType(int exprNumber, String type, String op) {
      // considering the required types based on the operation
      if(op.equals("&") || op.equals("|")) {
         type = "boolean";
      }
      else if(op.equals(">") || op.equals("<")) {
         type = "int";
      }
      else if(op.equals("+") || op.equals("-")) {
         type = "int";
      }
      else if(op.equals("*") || op.equals("/")) {
         type = "int";
      }
      else if(op.equals("-")) {
         type = "int";
      }
      else if(op.equals("~")) {
         type = "boolean";
      }
      // type can be anything
      else {
         type = "Array";
      }
      expectType(exprNumber, type);
   }

   // prints information regarding the symbol
   public void print() {
      System.out.print("\nUNKNOWN EXPRESSIONS\n");
      if(table.size() == 0) {
         String msg = "No unknown expressions";
         System.out.println(String.format("%" + 3 + "s", msg));
         System.out.print("\n\n");
         return;
      }
      System.out.println(String.format("%-12s %-12s %-13s %-6s %-4s %-13s %-10s",
         "class name", "name", "type", "offset", "line", "expected type", "file")
      );

      for(UnknownSymbol s : table) {
         System.out.println(String.format("%-12s %-12s %-13s %-6s %-4s %-13s %-10s",
            s.className, s.name, s.type, s.offset, s.line_num, s.expectedType, s.file)
         );
      }
      System.out.print("\n\n");
   }
}
